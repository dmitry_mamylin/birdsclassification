def prepare_training_data(fast_train):
    from keras.preprocessing.image import ImageDataGenerator

    if fast_train:
        n_epochs = 1
        n_batches = 16
        train_datagen = ImageDataGenerator(
            #samplewise_center=True,
            #samplewise_std_normalization=True,
            #rescale=1.0 / 255.0,
            #rotation_range=0.,
            #width_shift_range=0.,
            #height_shift_range=0.,
            #shear_range=0.,
            #zoom_range=0.,
            data_format='channels_last')
    else:
        n_epochs = 1000
        n_batches = 32
        train_datagen = ImageDataGenerator(
            #samplewise_center=True,
            #samplewise_std_normalization=True, ####
            rescale=1.0 / 255.0,
            #rotation_range=15.0,
            rotation_range=2.0, ####
            width_shift_range=0.08,
            height_shift_range=0.08,
            shear_range=0.1,
            zoom_range=0.1,
            fill_mode='nearest',
            horizontal_flip=True,
            data_format='channels_last')
        validation_datagen = ImageDataGenerator(
            #samplewise_center=True,
            #samplewise_std_normalization=True, ####
            rescale=1.0 / 255.0,
            #rotation_range=15.0,
            #rotation_range=4.0, ####
            width_shift_range=0.08,
            height_shift_range=0.08,
            shear_range=0.1,
            zoom_range=0.1,
            fill_mode='nearest',
            horizontal_flip=True,
            data_format='channels_last')

    if fast_train:
        return n_epochs, n_batches, train_datagen
    else:
        return n_epochs, n_batches, train_datagen, validation_datagen


def train_test_split(X, y):
    import numpy as np

    VALIDATION_SIZE = 150
    TRAIN_SIZE = X.shape[0] - VALIDATION_SIZE
    n_classes = y.shape[1]

    np.random.seed(42)

    indices = np.arange(y.shape[0])
    np.random.shuffle(indices)
    indices_validation = indices[:VALIDATION_SIZE]
    indices_train = indices[VALIDATION_SIZE:]

    return X[indices_train], y[indices_train], X[indices_validation], y[indices_validation]


def prepare_pretrained_model(input_shape):
    from keras.layers import Input
    from keras.applications.inception_v3 import InceptionV3
    from keras.applications.resnet50 import ResNet50

    input_layer = Input(input_shape)
    model = ResNet50(include_top=False, weights='imagenet', input_tensor=input_layer)

    for layer in model.layers:
        layer.trainable = False

    return model


def train_complete_model(X, y, fast_train):
    from math import ceil
    from keras.optimizers import SGD
    from keras.layers import GlobalAveragePooling2D, Dense, Dropout
    from keras.models import Model

    # DEBUG
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)
    #######

    n_classes = y.shape[1]
    if fast_train:
        n_samples = X.shape[0]
        n_epochs, n_batches, train_datagen = prepare_training_data(fast_train)
        n_steps_per_epoch = int(ceil(n_samples / n_batches))
    else:
        X, y, X_valid, y_valid = train_test_split(X, y)
        n_train_samples, n_validation_samples = X.shape[0], X_valid.shape[0]

        n_epochs, n_batches, train_datagen, validation_datagen = prepare_training_data(fast_train)
        n_steps_per_epoch = int(ceil(n_train_samples / n_batches))
        n_validation_steps = int(ceil(n_validation_samples / n_batches))
        validation_generator = validation_datagen.flow(X_valid, y_valid, batch_size=n_batches, seed=2)
    train_generator = train_datagen.flow(X, y, batch_size=n_batches, seed=2)

    pretrained_model = prepare_pretrained_model(X.shape[1:])
    x = pretrained_model.output
    x = GlobalAveragePooling2D()(x)
    #x = Dense(1024, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(n_classes, activation='softmax')(x)
    model = Model(inputs=pretrained_model.input, outputs=x)
    if not fast_train:
        pre_train_epochs = 20
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
        model.fit_generator(train_generator, steps_per_epoch=n_steps_per_epoch, epochs=pre_train_epochs)
    for layer in pretrained_model.layers[-15:]:
        layer.trainable = True

    model.compile(SGD(lr=1e-5, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
    if fast_train:
        model.fit_generator(train_generator, steps_per_epoch=n_steps_per_epoch, epochs=n_epochs)
    else:
        model.fit_generator(train_generator, steps_per_epoch=n_steps_per_epoch, epochs=n_epochs,
            validation_data=validation_generator, validation_steps=n_validation_steps)
    model.save('birds_model.hdf5')


def read_data(img_dir, gt=None):
    import numpy as np
    from skimage.io import imread_collection
    from skimage.transform import resize
    from ntpath import basename
    from os import listdir
    from os.path import join

    images = imread_collection(join(img_dir, '*.jpg'), conserve_memory=True)

    SAMPLES_COUNT = len(images.files)
    IMAGE_SHAPE = (224, 224, 3)
    if gt is not None:
        N_CLASSES = 50
        y_train = np.zeros((SAMPLES_COUNT, N_CLASSES), dtype=np.float32)

    x_train = np.ndarray((SAMPLES_COUNT,) + IMAGE_SHAPE, dtype=np.float32)
    sample = 0
    filenames = []
    for image, filename in zip(images, images.files):
        filename = basename(filename)
        filenames.append(filename)
        x_train[sample, :] = resize(image, IMAGE_SHAPE, mode='reflect').astype(np.float32)
        if gt is not None:
            y_train[sample, gt[filename]] = 1.0
        sample += 1

    if gt is not None:
        return x_train, y_train, filenames
    return x_train, filenames


def train_classifier(gt, img_dir, fast_train):
    X, y, _ = read_data(img_dir, gt)
    train_complete_model(X, y, fast_train)


def classify(model, img_dir):
    import numpy as np

    X, filenames = read_data(img_dir)
    y = np.argmax(model.predict(X), axis=1)

    return {filename: class_id for filename, class_id in zip(filenames, y)}
