def make_predictions(X, model):
    return model.predict(X)


def prepare_training_data(fast_train):
    from keras.preprocessing.image import ImageDataGenerator

    if fast_train:
        n_epochs = 1
        n_batches = 16
        train_datagen = ImageDataGenerator(
            samplewise_center=True,
            samplewise_std_normalization=True,
            #rescale=1.0 / 255.0,
            rotation_range=0.,
            width_shift_range=0.,
            height_shift_range=0.,
            shear_range=0.,
            zoom_range=0.,
            data_format='channels_last')
    else:
        n_epochs = 100
        n_batches = 32
        train_datagen = ImageDataGenerator(
            #samplewise_center=True,
            #samplewise_std_normalization=True, ####
            #rescale=1.0 / 255.0,
            #rotation_range=15.0,
            #rotation_range=4.0, ####
            width_shift_range=0.08,
            height_shift_range=0.08,
            shear_range=0.1,
            zoom_range=0.1,
            fill_mode='nearest',
            horizontal_flip=True,
            data_format='channels_last')

    return n_epochs, n_batches, train_datagen


def prepare_top_model(input_shape, n_classes):
    from keras.layers import Dense, Dropout, Flatten
    from keras.models import Sequential

    model = Sequential()
    model.add(Flatten(input_shape=input_shape))
    #model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(n_classes, activation='softmax'))

    return model


def train_top_model(X, y, model):
    from keras.optimizers import RMSprop
    from math import ceil

    n_samples = X.shape[0]
    n_classes = y.shape[1]
    n_batches = 32
    n_epochs = 500

    model.compile(RMSprop(), loss='categorical_crossentropy', metrics=['accuracy'])
    model.fit(X, y, batch_size=n_batches, epochs=n_epochs)


def load_top_model_weights(model, weights_path='top_model_weights.w'):
    model.load_weights(weights_path)


def prepare_pretrained_model(input_shape):
    from keras.layers import Input
    from keras.applications.resnet50 import ResNet50
    from keras.applications.inception_v3 import InceptionV3

    input_layer = Input(input_shape)
    #model = ResNet50(include_top=False, weights='imagenet', input_tensor=input_layer, pooling=None)
    model = InceptionV3(include_top=False, weights='imagenet', input_tensor=input_layer)

    for layer in model.layers:
        layer.trainable = False

    return model


def train_complete_model(X, y, fast_train):
    from math import ceil
    from keras.optimizers import SGD
    from keras.layers import GlobalAveragePooling2D, Dense
    from keras.models import Model

    # DEBUG
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)
    #######

    n_samples = X.shape[0]
    n_classes = y.shape[1]
    n_epochs, n_batches, train_datagen = prepare_training_data(fast_train)
    n_steps_per_epoch = int(ceil(n_samples / n_batches))
    train_generator = train_datagen.flow(X, y, batch_size=n_batches, seed=2)

    pretrained_model = prepare_pretrained_model(X.shape[1:])
    #top_model = prepare_top_model(pretrained_model.output_shape[1:], n_classes)
    x = pretrained_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    x = Dense(n_classes, activation='softmax')(x)
    model = Model(inputs=pretrained_model.input, outputs=x)
    if not fast_train:
        #train_top_model(make_predictions(X, pretrained_model), y, top_model)
        pre_train_epochs = 10
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
        model.fit_generator(train_generator, steps_per_epoch=n_steps_per_epoch, epochs=pre_train_epochs)
    for layer in pretrained_model.layers[-20:]:
        layer.trainable = True

    #model = Sequential()
    #model.add(pretrained_model)
    #model.add(top_model)
    #model.compile(SGD(lr=1e-3, momentum=0.9),
    model.compile(SGD(lr=1e-3, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
    model.fit_generator(train_generator, steps_per_epoch=n_steps_per_epoch, epochs=n_epochs)
    model.save('birds_model.hdf5')


def read_data(img_dir, gt=None):
    import numpy as np
    from skimage.io import imread_collection
    from skimage.transform import resize
    from ntpath import basename
    from os import listdir
    from os.path import join

    images = imread_collection(join(img_dir, '*.jpg'), conserve_memory=True)

    SAMPLES_COUNT = len(images.files)
    IMAGE_SHAPE = (224, 224, 3)
    if gt is not None:
        N_CLASSES = 50
        y_train = np.zeros((SAMPLES_COUNT, N_CLASSES), dtype=np.float32)

    x_train = np.ndarray((SAMPLES_COUNT,) + IMAGE_SHAPE, dtype=np.float32)
    sample = 0
    filenames = []
    for image, filename in zip(images, images.files):
        filename = basename(filename)
        filenames.append(filename)
        x_train[sample, :] = resize(image, IMAGE_SHAPE, mode='reflect').astype(np.float32)
        if gt is not None:
            y_train[sample, gt[filename]] = 1.0
        sample += 1

    if gt is not None:
        return x_train, y_train, filenames
    return x_train, filenames


def train_classifier(gt, img_dir, fast_train):
    X, y, _ = read_data(img_dir, gt)
    train_complete_model(X, y, fast_train)


def classify(model, img_dir):
    import numpy as np

    X, filenames = read_data(img_dir)
    y = np.argmax(make_predictions(X, model), axis=1)

    return {filename: class_id for filename, class_id in zip(filenames, y)}
